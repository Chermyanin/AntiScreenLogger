﻿using System.Windows.Forms;

namespace AntiScreenLogger
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            
            foreach (var screen in Screen.AllScreens)
            {
                CreateAntiCaptureForm(screen);
            }
        }

        private static void CreateAntiCaptureForm(Screen screen)
        {
            var captureForm = new AntiScreenCaptureForm();
            captureForm.SetSize(screen.Bounds.Location, screen.Bounds.Size);
            captureForm.Location = screen.Bounds.Location;
            captureForm.Show();
            captureForm.EnableAntiScreen = true;
        }
    }

}
