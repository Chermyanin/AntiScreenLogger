﻿using System;
using System.Runtime.InteropServices;
// ReSharper disable InconsistentNaming

namespace AntiScreenLogger
{
    public class Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫
    {
        [DllImport("user32.dll", EntryPoint = "SetWindowLong")]
        public static extern int SetWindowLong(
            IntPtr hWnd,
            Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.WindowAttributeOffset nIndex,
            int dwNewLong);

        [DllImport("user32.dll", EntryPoint = "SetLayeredWindowAttributes")]
        public static extern bool SetLayeredWindowAttributes(
            IntPtr hWnd,
            COLORREF crKey,
            byte bAlpha,
            SetLayeredWindowAttributesMode dwFlags);

        [DllImport("user32.dll", EntryPoint = "SetWindowDisplayAffinity")]
        public static extern bool SetWindowDisplayAffinity(
            IntPtr hWnd,
            DisplayAffinity dwAffinity);

        [DllImport("user32.dll", EntryPoint = "GetWindowDisplayAffinity")]
        public static extern bool GetWindowDisplayAffinity(
            IntPtr hWnd,
            ref DisplayAffinity dwAffinity);

        [DllImport("dwmapi.dll", EntryPoint = "DwmEnableComposition")]
        public static extern void DwmEnableComposition(
            DWM_COMPOSITION_MODE uCompositionAction);

        [DllImport("dwmapi.dll", EntryPoint = "DwmIsCompositionEnabled")]
        public static extern int DwmIsCompositionEnabled(ref bool pfEnabled);

        [DllImport("user32.dll", EntryPoint = "GetWindowLong", SetLastError = true)]
        public static extern int GetWindowLong(
            IntPtr hWnd,
            Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.WindowAttributeOffset nIndex);

        [DllImport("dwmapi.dll", EntryPoint = "DwmExtendFrameIntoClientArea")]
        public static extern void DwmExtendFrameIntoClientArea(
            IntPtr hWnd,
            ref Margins pMarInset);

        [DllImport("user32.dll", EntryPoint = "FlashWindow")]
        public static extern bool FlashWindow(
            IntPtr hWnd,
            bool bInvert);

        [Flags]
        public enum SetLayeredWindowAttributesMode:uint
        {
            LWA_ALPHA = 2,
            LWA_COLORKEY = 1
        }

        public struct Margins
        {
            public int cxLeftWidth;
            public int cxRightWidth;
            public int cyTopHeight;
            public int cyBottomHeight;

            public Margins(
                int leftWidth,
                int rightWidth,
                int topHeight,
                int bottomHeight)
            {
                cxLeftWidth = leftWidth;
                cxRightWidth = rightWidth;
                cyTopHeight = topHeight;
                cyBottomHeight = bottomHeight;
            }
        }

        [Flags]
        public enum DWM_COMPOSITION_MODE : uint
        {
            DWM_EC_DISABLECOMPOSITION = 0,
            DWM_EC_ENABLECOMPOSITION = 1,
        }

        [Flags]
        public enum DisplayAffinity : uint
        {
            None = 0,
            Monitor = 1,
        }

        public struct COLORREF
        {
            public byte R;
            public byte G;
            public byte B;

            public override string ToString()
            {
                return ToString("{0}{1}{2}", R, G, B);
            }

            private static string ToString(
                [In] string obj0,
                [In] object obj1,
                [In] object obj2,
                [In] object obj3)
            {
                return string.Format(obj0, obj1, obj2, obj3);
            }
        }

        public enum WS_EXTENDED_STYLE
        {
            WS_EX_TRANSPARENT =  0x00000020,
            WS_EX_LAYERED = 0x00080000
        }

        public enum WindowAttributeOffset:int
        {
            GWL_USERDATA = -21,
            GWL_EXSTYLE = -20, //https://docs.microsoft.com/ru-ru/windows/desktop/winmsg/extended-window-styles
            GWL_STYLE = -16,
            GWL_ID = -12,
            GWL_HINSTANCE = -6,
            GWL_WNDPROC = -4
        }
    }
}
