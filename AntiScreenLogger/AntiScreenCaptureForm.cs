using System;
using System.Drawing;
using System.Windows.Forms;

namespace AntiScreenLogger
{
    internal partial class AntiScreenCaptureForm : Form
    {
        private bool _isAntiScreenEnabled;
        public Point CurrentLocation;
        public Size CurrentSize;

        public AntiScreenCaptureForm()
        {
            InitializeComponent();
        }

        public bool EnableAntiScreen
        {
            get => _isAntiScreenEnabled;
            set
            {
                if (_isAntiScreenEnabled == value)
                    return;
                _isAntiScreenEnabled = value;
                if (_isAntiScreenEnabled) EnableScreenProtection();
                else DisableScreenProtection();
            }
        }

        public void SetSize(Point beginPoint, Size size)
        {
            CurrentLocation = beginPoint;
            CurrentSize = size;
            MinimumSize = CurrentSize;
            MaximumSize = CurrentSize;
        }

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/desktop/api/winuser/nf-winuser-setwindowdisplayaffinity
        /// </summary>
        public void EnableScreenProtection()
        {
            if (!DwmMethodsHelper.IsCompositionEnabled())
            {
                Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.DwmEnableComposition(Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.DWM_COMPOSITION_MODE.DWM_EC_ENABLECOMPOSITION);
            }

            if (DwmMethodsHelper.GetWindowDisplayAffinity(Handle) ==
                Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.DisplayAffinity.None)
            {
                Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.SetWindowDisplayAffinity(Handle, Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.DisplayAffinity.Monitor);
            }
        }

        public void DisableScreenProtection()
        {
            if (!DwmMethodsHelper.IsCompositionEnabled())
                return;

            if (DwmMethodsHelper.GetWindowDisplayAffinity(Handle) != Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.DisplayAffinity.Monitor)
            {
                Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.SetWindowDisplayAffinity(Handle, Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.DisplayAffinity.None);
            }
        }

        private void AntiScreenCaptureForm_OnLoad(object sender, EventArgs e)
        {
            SetHitTestTransparency();
           SetOpaque();
            CurrentSize = Size;
            MinimumSize = CurrentSize;
            MaximumSize = CurrentSize;
        }

        private void SetOpaque()
        {
            var color = new Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.COLORREF();
            Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.SetLayeredWindowAttributes(Handle, color, byte.MaxValue,
                Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.SetLayeredWindowAttributesMode.LWA_ALPHA);
            Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.FlashWindow(Handle, true);
        }

        /// <summary>
        /// https://devblogs.microsoft.com/oldnewthing/20121217-00/?p=5823
        /// </summary>
        private void SetHitTestTransparency()
        {
            var extendedStyles =
                Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.GetWindowLong(Handle, Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.WindowAttributeOffset.GWL_EXSTYLE);
            Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.SetWindowLong(Handle, Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.WindowAttributeOffset.GWL_EXSTYLE,
                extendedStyles | (int) Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.WS_EXTENDED_STYLE.WS_EX_LAYERED |
                (int) Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.WS_EXTENDED_STYLE.WS_EX_TRANSPARENT);
        }

        private void AntiScreenCaptureForm_OnPaint(
            object sender,
            PaintEventArgs e)
        {
            BlurWindow();
        }

        /// <summary>
        /// https://docs.microsoft.com/ru-ru/windows/desktop/dwm/blur-ovw#extending-the-window-frame-into-the-client-area
        /// </summary>
        private void BlurWindow()
        {
            var margins = new Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.Margins(0, 0, Width, Height);
            Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.DwmExtendFrameIntoClientArea(Handle, ref margins);
        }

        private void AntiScreenCaptureForm_OnLocationChanged(object sender, EventArgs e)
        {
            if (Location != CurrentLocation)
            {
                Location = CurrentLocation;
            }
        }

        private void AntiScreenCaptureForm_OnSizeChanged(object sender, EventArgs e)
        {
            if (Size != CurrentSize)
            {
                Size = CurrentSize;
                MinimumSize = CurrentSize;
                MaximumSize = CurrentSize;
            }
        }

        private void AntiScreenCaptureForm_OnVisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
                return;
            Visible = true;
        }

    }
}