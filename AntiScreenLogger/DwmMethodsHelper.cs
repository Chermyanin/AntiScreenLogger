using System;

namespace AntiScreenLogger
{
    public class DwmMethodsHelper
    {
        public static bool IsCompositionEnabled()
        {
            var result = false;
            Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.DwmIsCompositionEnabled(ref result);
            return result;
        }

        public static Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.DisplayAffinity GetWindowDisplayAffinity(IntPtr handle)
        {
            var result =
                default(Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.DisplayAffinity);
            Native‮⁪⁮‏⁯⁪‪⁭‏⁮‪⁭⁬​⁪‍⁫⁮⁫⁫.GetWindowDisplayAffinity(handle, ref result);
            return result;
        }
    }
}