﻿using System.Windows.Forms;

namespace AntiScreenLogger
{
    partial class AntiScreenCaptureForm
    {
        private void InitializeComponent()
        {
            this.LocationChanged += AntiScreenCaptureForm_OnLocationChanged;
            this.Paint += AntiScreenCaptureForm_OnPaint;
            this.SizeChanged += AntiScreenCaptureForm_OnSizeChanged;
            this.VisibleChanged += AntiScreenCaptureForm_OnVisibleChanged;
            this.Load += AntiScreenCaptureForm_OnLoad;
            this.SuspendLayout();
            // 
            // AntiScreenCaptureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1, 1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "AntiScreenCaptureForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        protected override CreateParams CreateParams
        {
            get
            {
                var createParams = base.CreateParams;
                createParams.ExStyle |= 128;
                return createParams;
            }
        }
    }
}
